package com.example.fitnesstracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DataBase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "FitnesSPRTrAcKer";

    private static final String TABLE_CONTACTS = "Exercises";

    // Имена на колоните на Таблицата
    private static final String KEY_ID = "id";
    private static final String KEY_MUSCLE_GROUP = "MuscleGroup";
    private static final String KEY_EXERCISE_NAME = "ExerciseName";
    private static final String KEY_CURRENT_WEIGHT = "CurrentWeight";
    private static final String KEY_BONUS_WEIGHT = "BonusWeight";
    private static final String KEY_PROGRESS_STEP = "ProgressStep";
    private static final String KEY_START_DATE = "StartDate";

    public DataBase(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS +
                "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_MUSCLE_GROUP + " TEXT,"
                + KEY_EXERCISE_NAME + " TEXT,"+ KEY_CURRENT_WEIGHT + " REAL,"
                + KEY_BONUS_WEIGHT + " REAL," + KEY_PROGRESS_STEP + " TEXT,"
                + KEY_START_DATE + " TEXT"+")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        // Повторно създаване на базата от данни
        onCreate(db);
    }

    public void addExercise(Exercise exercise){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MUSCLE_GROUP, exercise.getMuscleGroup());
        values.put(KEY_EXERCISE_NAME, exercise.getExerciseName());
        values.put(KEY_CURRENT_WEIGHT, exercise.getCurrentWeight());
        values.put(KEY_BONUS_WEIGHT, exercise.getBonusWeight());
        values.put(KEY_PROGRESS_STEP, exercise.getProgressStep());
        values.put(KEY_START_DATE, new SimpleDateFormat("dd/MM/yyyy").format(exercise.getStartDate()));
        db.insert(TABLE_CONTACTS,null,values);
        db.close();
    }

    public List<Exercise> getAllExercises(){
        List<Exercise> exerciseList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Exercise exercise = new Exercise();
                exercise.setId(Integer.parseInt(cursor.getString(0)));
                exercise.setMuscleGroup(cursor.getString(1));
                exercise.setExerciseName(cursor.getString(2));
                exercise.setCurrentWeight(Double.parseDouble(cursor.getString(3)));
                exercise.setBonusWeight(Double.parseDouble(cursor.getString(4)));
                exercise.setProgressStep(cursor.getString(5));
                try{
                    exercise.setStartDate(new SimpleDateFormat("dd/MM/yyyy")
                            .parse((cursor.getString(6))));
                }catch (ParseException e){
                    System.out.println(e.getMessage());
                }
                exerciseList.add(exercise);
            } while (cursor.moveToNext());
        }

        return updatingWeights(exerciseList);
    }
    private List<Exercise> updatingWeights(List<Exercise> currList){
        List<Exercise> updatedList = new ArrayList<>();
        Date currentDate = new Date();
        for (Exercise exercise : currList) {
            if(exercise.getStartDate().before(currentDate)){
                long timeDifference = currentDate.getTime() - exercise.getStartDate().getTime();
                long daysDifference = (timeDifference/(1000*60*60*24)) % 365;
                switch (exercise.getProgressStep().toLowerCase(Locale.ROOT)){
                    case "1 week":
                        if(daysDifference/7 >0){
                            long progress = daysDifference/7;
                            exercise.setCurrentWeight(exercise.getCurrentWeight() +
                                    (progress*exercise.getBonusWeight()));
                        }
                        break;
                    case "2 weeks":
                        if(daysDifference/14 >0){
                            long progress = daysDifference/14;
                            exercise.setCurrentWeight(exercise.getCurrentWeight() +
                                    (progress*exercise.getBonusWeight()));
                        }
                        break;
                    case "3 weeks":
                        if(daysDifference/21 >0){
                            long progress = daysDifference/21;
                            exercise.setCurrentWeight(exercise.getCurrentWeight() +
                                    (progress*exercise.getBonusWeight()));
                        }
                        break;
                    case "1 month":
                        if(daysDifference/30 >0){
                            long progress = daysDifference/30;
                            exercise.setCurrentWeight(exercise.getCurrentWeight() +
                                    (progress*exercise.getBonusWeight()));
                        }
                        break;

                }
                updatedList.add(exercise);
            }
        }
        return updatedList;
    }
}
