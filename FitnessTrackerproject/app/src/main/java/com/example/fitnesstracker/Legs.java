package com.example.fitnesstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class Legs extends AppCompatActivity {


    final DataBase db = new DataBase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.activity_legs);

        final TextView currentWeightsLegs= findViewById(R.id.autoCompleteLegs);
        List<Exercise> exercises = db.getAllExercises();
        final ImageButton backButton = findViewById(R.id.backButtonLegs);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        currentWeightsLegs.setTextIsSelectable(false);
        currentWeightsLegs.setMovementMethod(new ScrollingMovementMethod());
        currentWeightsLegs.setText("");
        for (Exercise exercise : exercises) {
            if (exercise.getMuscleGroup().equalsIgnoreCase("Legs")) {
                String log = currentWeightsLegs.getText() + "\n" + "    "+exercise.getExerciseName()
                        + ": " + exercise.getCurrentWeight() + " kg";
                currentWeightsLegs.setText(log);
            }
        }
        if (currentWeightsLegs.getText().toString().isEmpty()) {
            currentWeightsLegs.setText(R.string.emptyExerciseList);
        }
    }
}