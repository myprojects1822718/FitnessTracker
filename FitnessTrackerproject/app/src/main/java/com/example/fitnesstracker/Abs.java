package com.example.fitnesstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class Abs extends AppCompatActivity {

    final DataBase db = new DataBase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN /*| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY*/
        );
        setContentView(R.layout.activity_abs);

        final TextView currentWeightsAbs = findViewById(R.id.autoCompleteAbs);
        List<Exercise> exercises = db.getAllExercises();
        final ImageButton backButton = findViewById(R.id.backButtonAbs);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        currentWeightsAbs.setTextIsSelectable(false);
        currentWeightsAbs.setMovementMethod(new ScrollingMovementMethod());
        currentWeightsAbs.setText("");
        for (Exercise exercise : exercises) {
            if(exercise.getMuscleGroup().equalsIgnoreCase("Abs")){
                String log = currentWeightsAbs.getText() + "\n" + "     "+ exercise.getExerciseName()
                        + ": " + exercise.getCurrentWeight() + " kg";
                currentWeightsAbs.setText(log);
            }

        }
        if(currentWeightsAbs.getText().toString().isEmpty()){
            currentWeightsAbs.setText(R.string.emptyExerciseList);
    }

    }
}