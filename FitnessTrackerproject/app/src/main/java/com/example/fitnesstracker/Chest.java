package com.example.fitnesstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class Chest extends AppCompatActivity {

    final DataBase db = new DataBase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.activity_chest);

        final TextView currentWeightsChest = findViewById(R.id.autoCompleteChest);
        List<Exercise> exercises = db.getAllExercises();
        final ImageButton backButton = findViewById(R.id.backButtonChest);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        currentWeightsChest.setTextIsSelectable(false);
        currentWeightsChest.setLongClickable(false);
        currentWeightsChest.setMovementMethod(new ScrollingMovementMethod());
        currentWeightsChest.setText("");
        for (Exercise exercise : exercises) {
            if (exercise.getMuscleGroup().equalsIgnoreCase("Chest")) {
                String log = currentWeightsChest.getText() + "\n" + "   "+ exercise.getExerciseName()
                        + ": " + exercise.getCurrentWeight() + " kg";
                currentWeightsChest.setText(log);
            }
        }
        if (currentWeightsChest.getText().toString().isEmpty()) {
            currentWeightsChest.setText(R.string.emptyExerciseList);
        }

    }
}