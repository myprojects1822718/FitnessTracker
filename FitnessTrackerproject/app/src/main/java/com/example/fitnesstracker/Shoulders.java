package com.example.fitnesstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class Shoulders extends AppCompatActivity {

    final DataBase db = new DataBase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.activity_shoulders);

        final TextView currentWeightsShoulders = findViewById(R.id.autoCompleteShoulders);
        List<Exercise> exercises = db.getAllExercises();
        final ImageButton backButton = findViewById(R.id.backButtonShoulders);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        currentWeightsShoulders.setTextIsSelectable(false);
        currentWeightsShoulders.setMovementMethod(new ScrollingMovementMethod());
        currentWeightsShoulders.setText("");
        for (Exercise exercise : exercises) {
            if(exercise.getMuscleGroup().equalsIgnoreCase("Shoulders")){
                String log = currentWeightsShoulders.getText() + "\n" + "   "+ exercise.getExerciseName()
                        + ": " + exercise.getCurrentWeight() + " kg";
                currentWeightsShoulders.setText(log);
            }

        }
        if(currentWeightsShoulders.getText().toString().isEmpty()){
            currentWeightsShoulders.setText(R.string.emptyExerciseList);
        }
    }
}