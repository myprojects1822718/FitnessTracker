package com.example.fitnesstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class CurrentWeights extends AppCompatActivity {

    final DataBase db = new DataBase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.activity_current_weights);

        final Button chestButton = findViewById(R.id.buttonChest);
        final Button backButton = findViewById(R.id.buttonBack);
        final Button armsButton = findViewById(R.id.buttonArms);
        final Button absButton = findViewById(R.id.buttonAbs);
        final Button legsButton = findViewById(R.id.buttonLegs);
        final Button shouldersButton = findViewById(R.id.buttonShoulders);

        final ImageButton backPageButton = findViewById(R.id.backButtonChest);
        backPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        chestButton.setOnClickListener(view ->
                startActivity(new Intent(CurrentWeights.this, Chest.class)));
        backButton.setOnClickListener(view ->
                startActivity(new Intent(CurrentWeights.this, Back.class)));
        armsButton.setOnClickListener(view ->
                startActivity(new Intent(CurrentWeights.this, Arms.class)));
        absButton.setOnClickListener(view ->
                startActivity(new Intent(CurrentWeights.this, Abs.class)));
        legsButton.setOnClickListener(view ->
                startActivity(new Intent(CurrentWeights.this, Legs.class)));
        shouldersButton.setOnClickListener(view ->
                startActivity(new Intent(CurrentWeights.this, Shoulders.class)));
    }
}