package com.example.fitnesstracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Exercise {
    private int id;
    private String muscleGroup;
    private String exerciseName;
    private double currentWeight;
    private double bonusWeight;
    private String progressStep;
    private Date startDate;

    public Exercise() {
    }

    public Exercise(String muscleGroup, String exerciseName, double currentWeight, double bonusWeight, String progressStep, Date startDate) {
        this.muscleGroup = muscleGroup;
        this.exerciseName = exerciseName;
        this.currentWeight = currentWeight;
        this.bonusWeight = bonusWeight;
        this.progressStep = progressStep;
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate){
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(String muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public double getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(double currentWeight) {
        this.currentWeight = currentWeight;
    }

    public double getBonusWeight() {
        return bonusWeight;
    }

    public void setBonusWeight(double bonusWeight) {
        this.bonusWeight = bonusWeight;
    }

    public String getProgressStep() {
        return progressStep;
    }

    public void setProgressStep(String progressStep) {
        this.progressStep = progressStep;
    }

}
