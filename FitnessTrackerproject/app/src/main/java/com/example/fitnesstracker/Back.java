package com.example.fitnesstracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class Back extends AppCompatActivity {

    final DataBase db = new DataBase(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.activity_back);

        final TextView currentWeightsBack = findViewById(R.id.autoCompleteBack);
        List<Exercise> exercises = db.getAllExercises();
        final ImageButton backButton = findViewById(R.id.backButtonBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        currentWeightsBack.setTextIsSelectable(false);
        currentWeightsBack.setMovementMethod(new ScrollingMovementMethod());
        currentWeightsBack.setText("");
        for (Exercise exercise : exercises) {
            if (exercise.getMuscleGroup().equalsIgnoreCase("Back")) {
                String log = currentWeightsBack.getText() + "\n" + "    " + exercise.getExerciseName()
                        + ": " + exercise.getCurrentWeight() + " kg";
                currentWeightsBack.setText(log);
            }

        }
        if (currentWeightsBack.getText().toString().isEmpty()) {
            currentWeightsBack.setText(R.string.emptyExerciseList);
        }
    }
}