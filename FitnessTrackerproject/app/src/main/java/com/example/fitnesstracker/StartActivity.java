package com.example.fitnesstracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/*     Date startDate = new Date();
    String date_string = "29-2-2023";
    //Instantiating the SimpleDateFormat class
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    //Parsing the given String to Date object
    Date date = formatter.parse(date_string);
   Exercise exr1 = new Exercise("Chest", "BenchPress", 20, 1, "1 MONTH", date);
            Exercise exr2 = new Exercise("Back", "Pull-ups", 70, 1, "1 week", startDate);
            Exercise exr3 = new Exercise("Back", "Back extension", 15, 1, "1 month", startDate);
            Exercise exr4 = new Exercise("Back", "Reverse flye", 20, 1, "1 week", startDate);
            Exercise exr5 = new Exercise("Back", "Pull-down", 45, 1, "1 week", startDate);
            Exercise exr6 = new Exercise("Back", "T-bar", 30, 5, "1 week", startDate);
            Exercise exr7 = new Exercise("Back", "Dumbel Pull", 12.5, 1, "1 week", startDate);
            Exercise exr8 = new Exercise("Back", "Face pull", 20, 2.5, "2 weeks", startDate);
            Exercise exr9 = new Exercise("Back", "Pulley roll", 45, 2.5, "2 weeks", startDate);
            Exercise exr10 = new Exercise("Back", "Deadlift", 40, 2.5, "2 weeks", startDate);
            Exercise exr11 = new Exercise("Back", "Inverted row", 35, 1.5, "3 weeks", startDate);
*//*        db.addExercise(exr2);
    db.addExercise(exr3);
    db.addExercise(exr4);
    db.addExercise(exr5);
    db.addExercise(exr6);
    db.addExercise(exr7);
    db.addExercise(exr8);
    db.addExercise(exr9);
    db.addExercise(exr2);
    db.addExercise(exr2);
    db.addExercise(exr2);
    db.addExercise(exr10);
    db.addExercise(exr1);*/
public class StartActivity extends AppCompatActivity {

    private static final String CHANNEL_ID = "my_channel";
    private static final int NOTIFICATION_ID = 1;
    final DataBase db = new DataBase(this);

    public StartActivity() throws ParseException {
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );      setContentView(R.layout.activity_start);
        final Button journeyBeginning = (Button) findViewById(R.id.journeyBeginningButton);

        List<Exercise> exercises = db.getAllExercises();
        if (exercises.isEmpty()) {
            journeyBeginning.setText("Start your Progress");
        } else {
            journeyBeginning.setText("Continue your Progress");
        }
//        startService(new Intent(this, MyNotificationService.class));

        journeyBeginning.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                showNotification();
                startActivity(new Intent(StartActivity.this, MainActivity.class));
            }
        });
    }
    private void showNotification(){
          NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "My App Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        Intent intent = new Intent(this, MainActivity.class); // ?
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);


        Uri soundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.mixkit_cooking_stopwatch_alert_1792);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Fitness Progress Tracker")
                .setContentText("The Gym awaits you!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentIntent(pendingIntent)
                .setSound(soundUri)
                .setAutoCancel(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

}