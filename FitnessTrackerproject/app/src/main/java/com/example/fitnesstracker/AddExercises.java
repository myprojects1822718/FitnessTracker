package com.example.fitnesstracker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;

public class AddExercises extends AppCompatActivity {

    AlertDialog.Builder alertDialogBuilder;
    AlertDialog alertDialog;
    String[] muscleGroups = {"CHEST", "BACK", "ARMS", "ABS", "LEGS", "SHOULDERS"};
    String[] progressSteps = {"1 WEEK", "2 WEEKS","3 WEEKS", "1 MONTH"};

    ArrayAdapter<String> adapterMuscleGroups;
    ArrayAdapter<String> adapterProgressSteps;

    Spinner muscleGroupDropDown;
    TextView exerciseName;
    TextView startingWeight;
    TextView bonusWeight;
    Spinner progressStepsDropDown;
    Button addExerciseButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.activity_add_exercises);
        final DataBase db = new DataBase(this);
        final List<Exercise> exerciseList = db.getAllExercises();
        final ImageButton backButton = findViewById(R.id.backButtonAddExercise);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alertDialogBuilder = new AlertDialog.Builder(this);

        muscleGroupDropDown = findViewById(R.id.muscleGroupSpinner);
        exerciseName = findViewById(R.id.exerciseNameEditText);
        startingWeight = findViewById(R.id.currentWeightsEditText);
        bonusWeight = findViewById(R.id.bonusWeightsEditText);
        progressStepsDropDown = findViewById(R.id.progressStepSpinner);
        addExerciseButton = findViewById(R.id.addExercisebutton);

        adapterMuscleGroups = new ArrayAdapter<String>(this,
                androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, muscleGroups);
        muscleGroupDropDown.setAdapter(adapterMuscleGroups);

        adapterProgressSteps = new ArrayAdapter<String>(this,
                androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, progressSteps);
        progressStepsDropDown.setAdapter(adapterProgressSteps);


        addExerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if(exerciseName.getText().toString().isEmpty()){
                        throw new InputMismatchException();
                    }
                    db.addExercise(new Exercise(
                            muscleGroupDropDown.getSelectedItem().toString(), exerciseName.getText().toString(),
                            Double.parseDouble(startingWeight.getText().toString()),
                            Double.parseDouble(bonusWeight.getText().toString()),
                            progressStepsDropDown.getSelectedItem().toString(),
                            new Date()
                            )
                    );
                    ShowDialog("Exercise is Add !");
                    alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }catch (InputMismatchException e){
                    ShowDialogAndStayOnTheSamePage("Exercise name shouldn't be empty");
                    alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }catch (NumberFormatException e){
                    ShowDialogAndStayOnTheSamePage("Weights must be valid !");
                    alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }
        });
    }
    private void ShowDialogAndStayOnTheSamePage(String ss){
        alertDialogBuilder.setMessage(ss);
        alertDialogBuilder.setPositiveButton("Ок", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        alertDialog.cancel();
                    }
                }
        );
    }
    private void ShowDialog(String ss){
        alertDialogBuilder.setMessage(ss);
        alertDialogBuilder.setPositiveButton("Ок", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        startActivity(new Intent(AddExercises.this,
                                MainActivity.class));
                    }
                }
        );
    }
}