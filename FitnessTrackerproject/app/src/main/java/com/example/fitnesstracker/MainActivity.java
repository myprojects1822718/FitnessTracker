package com.example.fitnesstracker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    AlertDialog.Builder alertDialogBuilder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.activity_main);
        alertDialogBuilder = new AlertDialog.Builder(this);

        final DataBase db = new DataBase(this);
        final Button currentWeights = findViewById(R.id.buttonCurrentWeights);
        final Button addExercise = findViewById(R.id.buttonSetUpGoals);
        final Button resetProgress = findViewById(R.id.buttonRestartProgress);

        currentWeights.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CurrentWeights.class));
            }
        });
        addExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddExercises.class));
            }
        });
        resetProgress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                alertDialogBuilder.setMessage("Are you sure you want to reset your progress?" +
                                " \nYour exercises are going to be lost!")
                        .setTitle("Confirmation")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                db.onUpgrade(db.getReadableDatabase(), db.getReadableDatabase().getVersion(),
                                        db.getReadableDatabase().getVersion() + 1);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });
    }

}